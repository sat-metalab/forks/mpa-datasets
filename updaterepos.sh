#!/bin/bash

# To run this script for testing:
# - install dependencies listed in .gitlab-ci.yml under before_script section
# - find the value of KEY set in gitlab repository: Settings > CI/CD > Variables
# - adapt the value "..." of KEY to run the command lines below in a terminal: 
#   export KEY="..."
#   ./updaterepos.h

# List projects using their path with namespace
projects="sat-mtl/tools/livepose"

# Define architectures
archs="amd64 arm64"

# Define substring to match in package filenames
match="datasets"

# Cleanup tmp deb
find /tmp -maxdepth 1 -name "*.deb" -exec rm {} \;

# Download latest packages
echo -e "\e[0;32mDownload latest DEB packages...\e[0m"
mkdir -p pkgs/debs
for p in $projects; do
  p=`echo $p | sed 's/\//%2F/g'`
  
  project=`curl -s https://gitlab.com/api/v4/projects/$p/`;
  name=`echo $project | jq .name`
  echo -e "\e[0;32mDownload latest DEB package for project $name...\e[0m"

  releases=`curl -s https://gitlab.com/api/v4/projects/$p/releases`;
  rs=`echo $releases | jq length`;
  r=0;
  url="";
  found=0;
  while [ $r -lt $((rs)) ]
  do
    links=`echo $releases | jq .[$r].assets.links`;
    lss=`echo $links | jq length`;
    l=0;
    while [ $l -lt $((lss)) ]
    do
      filename=`echo $links| jq -r .[$l].name`
      url=`echo $links| jq -r .[$l].url`
      echo "Link name $filename url $url";
      if [[ $filename == *"$match"* ]]; then
        echo -e "\e[0;32mDownloading $name package $name from $url...\e[0m"
        echo curl --fail -L -o "/tmp/$filename" -s $url
        curl --fail -L -o "/tmp/$filename" -s $url \
         ||  { echo "Failed to download $name package $name from $url"; break; }
        tar -xf "/tmp/$filename" -C /tmp;
        found=1;
        break;
      fi
      echo "Link: $l"
      ((l++))
    done
    if [[ $found == 1 ]]; then
      break;
    fi
    echo "Release: $r"
    ((r++))
  done;
done;

# reprepro configuration
mkdir -p pkgs/debs/conf
touch pkgs/debs/conf/option
touch pkgs/debs/conf/distributions
echo "Codename: sat-metalab" > pkgs/debs/conf/distributions
echo "Components: main" >> pkgs/debs/conf/distributions
echo "Architectures: $archs" >> pkgs/debs/conf/distributions
echo 'SignWith: 5A44DBAC' >> pkgs/debs/conf/distributions


echo -e "\e[0;32mSign the repositories\e[0m"
if [ ! -d "keys" ]; then
  # extract the public and private GPG keys from encrypted archive keys.tar with 
  # the secret openssl pass KEY, which is stored in GitlabCI variables
  openssl aes-256-cbc -d -in keys.tar.enc -out keys.tar -k $KEY
  tar xvf keys.tar
  #signing the repository metadata with my personal GPG key
  gpg2 --import keys/pub.gpg && gpg2 --import keys/priv.gpg
  expect -c "spawn gpg2 --edit-key F5F78AA6969F99623EF579D9E422824B62B631DC trust quit; send \"5\ry\r\"; expect eof"
fi

if [ 0 -lt $(ls /tmp/*.deb 2>/dev/null | wc -w) ]; then
  debsigs --sign=origin -k 5A44DBAC /tmp/*.deb
fi

if [ 0 -lt $(ls /tmp/*.deb 2>/dev/null | wc -w) ]; then
  echo -e "\e[0;32mGenerating new deb repository:"
  reprepro -V -b pkgs/debs includedeb sat-metalab /tmp/*deb
  if ! [ -d pkgs/debs/dists ]; then
    echo "Error"
    exit 1
  fi
else
  echo -e "\e[0;32mGenerating empty repository:"
  reprepro -V -b pkgs/debs export sat-metalab
fi

echo -e "\e[0;32mDeploy to Gitlab Pages...\e[0m"
